# hadoop-formula/hadoop/users/hdfs.sls
hdfs:
  user.present:
    - uid: 6001
    - gid: 6000
    - groups:
      - hadoop
    - home: /mnt/nfs/users/hdfs
    - password: $6$hadoopformula$ipmD0PvltHzj5e/QMmag5rqRhdVi5z781uvNr.MtXYXmzKlM7LWO3JRl9R2CwbcVnWl6wHlZNtWoUiNpOBl5p.