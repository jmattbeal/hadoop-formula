# hadoop-formula/hadoop/users/yarn.sls
yarn:
  user.present:
    - uid: 6002
    - gid: 6000
    - groups:
      - hadoop
    - home: /mnt/nfs/users/yarn
    - password: $6$hadoopformula$ipmD0PvltHzj5e/QMmag5rqRhdVi5z781uvNr.MtXYXmzKlM7LWO3JRl9R2CwbcVnWl6wHlZNtWoUiNpOBl5p.