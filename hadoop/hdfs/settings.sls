{%- set p  = salt['pillar.get']('hdfs', {}) %}
{%- set pc = p.get('config', {}) %}
{%- set g  = salt['grains.get']('hdfs', {}) %}
{%- set gc = g.get('config', {}) %}

{%- set namenode_target     = g.get('namenode_target', p.get('namenode_target', 'roles:hadoop_master')) %}
{%- set datanode_target     = g.get('datanode_target', p.get('datanode_target', 'roles:hadoop_slave')) %}
{%- set pnn_target          = g.get('pnn_target', p.get('pnn_target', 'roles:hadoop_primary_namenode')) %}
{%- set snn_target          = g.get('snn_target', p.get('snn_target', 'roles:hadoop_secondary_namenode')) %}

# this is a deliberate duplication as to not re-import hadoop/settings multiple times
{%- set targeting_method    = salt['grains.get']('hadoop:targeting_method', salt['pillar.get']('hadoop:targeting_method', 'grain')) %}
{%- set pnn_host            = salt['mine.get'](pnn_target, 'network.interfaces', expr_form=targeting_method)|first %}
{%- set snn_host            = salt['mine.get'](snn_target, 'network.interfaces', expr_form=targeting_method)|first %}
{%- set datanode_hosts      = salt['mine.get'](datanode_target, 'network.interfaces', expr_form=targeting_method).keys() %}
{%- set datanode_count      = datanode_hosts|count() %}
{%- set pnn_port            = gc.get('primarynamenode_port', pc.get('primarynamenode_port', '8020')) %}
{%- set pnn_http_port       = gc.get('primarynamenode_http_port', pc.get('primarynamenode_http_port', '50070')) %}
{%- set snn_http_port       = gc.get('secondarynamenode_http_port', pc.get('secondarynamenode_http_port', '50090')) %}
{%- set data_disks          = salt['grains.get']('hdfs_data_disks', ['/data']) %}
{%- set namenode_disks      = salt['grains.get']('hdfs_namenode_disks', ['/namenode']) %}
{%- set hdfs_repl_override  = gc.get('replication', pc.get('replication', 'x')) %}
{%- set tmp_dir             = '/tmp' %}

{%- if hdfs_repl_override == 'x' %}
{%- if datanode_count >= 3 %}
{%- set replicas = '3' %}
{%- elif datanode_count == 2 %}
{%- set replicas = '2' %}
{%- else %}
{%- set replicas = '1' %}
{%- endif %}
{%- endif %}

{%- if hdfs_repl_override != 'x' %}
{%- set replicas = hdfs_repl_override %}
{%- endif %}

{%- set config_hdfs_site = gc.get('hdfs-site', pc.get('hdfs-site', {})) %}

{%- set is_namenode = salt['match.' ~ targeting_method](namenode_target) %}
{%- set is_datanode = salt['match.' ~ targeting_method](datanode_target) %}
{%- set is_pnn = salt['match.' ~ targeting_method](pnn_target) %}
{%- set is_snn = salt['match.' ~ targeting_method](snn_target) %}

{%- set hdfs = {} %}
{%- do hdfs.update({ 'data_disks'                  : data_disks,
                     'namenode_disks'              : namenode_disks,
                     'pnn_host'                    : pnn_host,
                     'snn_host'                    : snn_host,
                     'datanode_hosts'              : datanode_hosts,
                     'pnn_port'                    : pnn_port,
                     'pnn_http_port'               : pnn_http_port,
                     'is_namenode'                 : is_namenode,
                     'is_datanode'                 : is_datanode,
                     'is_pnn'                      : is_pnn,
                     'is_snn'                      : is_snn,
                     'snn_http_port'               : snn_http_port,
                     'replicas'                    : replicas,
                     'datanode_count'              : datanode_count,
                     'config_hdfs_site'            : config_hdfs_site,
                     'tmp_dir'                     : tmp_dir,
                     'load'                        : load,
                   }) %}
