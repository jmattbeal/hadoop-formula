# hadoop-formula/hadoop/hdfs/init.sls

{%- from 'hadoop/settings.sls' import hadoop with context %}
{%- from 'hadoop/hdfs/settings.sls' import hdfs with context %}

{%- set username = 'hdfs' %}
{%- set uid = '6001' %}

# every node can advertise any JBOD drives to the framework by setting the hdfs_data_disk grain
{%- set hdfs_data_disks = hdfs.data_disks %}
{%- set hdfs_namenode_disks = hdfs.namenode_disks %}
{%- set test_folder = hdfs_data_disks|first() + '/hdfs/nn/current' %}

include:
  - hadoop.users.hdfs
  
{{ hadoop.hadoop_config }}/core-site.xml:
  file.managed:
    - source: salt://hadoop/conf/hdfs/core-site.xml
    - template: jinja
    - mode: 644

{% if hdfs.is_namenode %}
{% for disk in hdfs_namenode_disks %}
/data/{{ disk }}/hdfs:
  file.directory:
    - user: root
    - group: root
    - makedirs: True
/data/{{ disk }}/hdfs/name:
  file.directory:
    - user: {{ username }}
    - group: hadoop
    - makedirs: True
/data/{{ disk }}/hdfs/secondary:
  file.directory:
    - user: {{ username }}
    - group: hadoop
    - makedirs: True
{% endfor %}
{% endif %}

{% if hdfs.is_datanode %}
{% for disk in hdfs_data_disks %}
/data/{{ disk }}/hdfs:
  file.directory:
    - user: root
    - group: root
    - makedirs: True
/data/{{ disk }}/hdfs/data:
  file.directory:
    - user: {{ username }}
    - group: hadoop
    - makedirs: True
{% endfor %}
{% endif %}

{%- if hdfs.tmp_dir != '/tmp' %}
{{ hdfs.tmp_dir }}:
  file.directory:
    - user: {{ username }}
    - group: hadoop
    - makedirs: True
    - mode: '1775'
{% endif %}

{{ hadoop.hadoop_config }}/hdfs-site.xml:
  file.managed:
    - source: salt://hadoop/conf/hdfs/hdfs-site.xml
    - template: jinja
    - mode: 644

{{ hadoop.hadoop_config }}/slaves:
  file.managed:
    - mode: 644
    - contents: |
{%- for slave in hdfs.datanode_hosts %}
        {{ slave }}
{%- endfor %}

{{ hadoop.hadoop_config }}/dfs.hosts:
  file.managed:
    - mode: 644
    - contents: |
{%- for slave in hdfs.datanode_hosts %}
        {{ slave }}
{%- endfor %}

{{ hadoop.hadoop_config }}/dfs.hosts.exclude:
  file.managed



{% if hdfs.is_namenode %}

format-namenode:
  cmd.run:
    - name: {{ hadoop.hadoop_home }}/bin/hdfs --config {{ hadoop.hadoop_config }} namenode -format
    - user: hdfs
    - unless: test -d {{ test_folder }}

/etc/init.d/hadoop-namenode:
  file.managed:
    - source: salt://hadoop/files/{{ hadoop.initscript }}
    - user: root
    - group: root
    - mode: '755'
    - template: jinja
    - context:
      hadoop_svc: namenode
      hadoop_user: hdfs
      hadoop_home: {{ hadoop.hadoop_home }}

/etc/init.d/hadoop-secondarynamenode:
  file.managed:
    - source: salt://hadoop/files/{{ hadoop.initscript }}
    - user: root
    - group: root
    - mode: '755'
    - template: jinja
    - context:
      hadoop_svc: secondarynamenode
      hadoop_user: hdfs
      hadoop_home: {{ hadoop.hadoop_home }}
{% endif %}


{% if hdfs.is_datanode %}
/etc/init.d/hadoop-datanode:
  file.managed:
    - source: salt://hadoop/files/{{ hadoop.initscript }}
    - user: root
    - group: root
    - mode: '755'
    - template: jinja
    - context:
      hadoop_svc: datanode
      hadoop_user: hdfs
      hadoop_home: {{ hadoop.hadoop_home }}
{% endif %}



{% if hdfs.is_namenode or hdfs.is_datanode %}
hdfs-services:
  service.running:
    - enable: True
    - names:
{% if hdfs.is_pnn %}
      - hadoop-namenode
{% elif hdfs.is_snn %}
      - hadoop-secondarynamenode
{% else %}
      - hadoop-datanode
{% endif %}

{% endif %}

  