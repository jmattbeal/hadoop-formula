{% set p  = salt['pillar.get']('hadoop', {}) %}
{% set pc = p.get('config', {}) %}
{% set g  = salt['grains.get']('hadoop', {}) %}
{% set gc = g.get('config', {}) %}

{%- set log_root         = '/var/log/hadoop' %}
{%- set java_home        = salt['grains.get']('java_home', salt['pillar.get']('java_home', '/usr/lib/jvm/java-8-oracle')) %}
{%- set hadoop_home      = '/usr/lib/hadoop-2.7.1' %}
{%- set hadoop_config    = '/etc/hadoop/conf' %}
{%- set initscript       = 'hadoop.init' %}

{%- set hadoop = {} %}
{%- do hadoop.update( {   
                          'java_home'        : java_home,
                          'hadoop_home'      : hadoop_home,
                          'hadoop_config'    : hadoop_config,
                          'log_root'         : log_root,
                          'initscript'       : initscript
                      }) %}
