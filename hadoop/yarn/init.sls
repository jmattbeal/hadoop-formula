{%- from "hadoop/settings.sls" import hadoop with context %}
{%- from "hadoop/yarn/settings.sls" import yarn with context %}
{%- from "hadoop/hdfs/settings.sls" import hdfs with context %}
{%- from 'hadoop/hdfs_mkdir_macro.sls' import hdfs_mkdir with context %}

include:
  - hadoop.users.yarn

{% set username = 'yarn' %}
{% set uid = '6002' %}

{{ hadoop.hadoop_config }}/yarn-site.xml:
  file.managed:
    - source: salt://hadoop/conf/yarn/yarn-site.xml
    - mode: 644
    - user: root
    - template: jinja

{{ hadoop.hadoop_config }}/capacity-scheduler.xml:
  file.managed:
    - source: salt://hadoop/conf/yarn/capacity-scheduler.xml
    - mode: 644
    - user: root
    - template: jinja
    
{{ hadoop.hadoop_config }}/slaves:
  file.managed:
    - mode: 644
    - contents: |
{%- for slave in hdfs.datanode_hosts %}
        {{ slave }}
{%- endfor %}

{% if yarn.is_resourcemanager %}

{% for disk in yarn.yarn_resource_mgmt_disks %}
/data/{{ disk }}/yarn:
  file.directory:
    - user: root
    - group: root
    - makedirs: True
/data/{{ disk }}/yarn/local:
  file.directory:
    - user: yarn
    - group: hadoop
    - require:
      - file: /data/{{ disk }}/yarn
/data/{{ disk }}/yarn/logs:
  file.directory:
    - user: yarn
    - group: hadoop
    - require:
      - file: /data/{{ disk }}/yarn
{% endfor %}

/etc/init.d/hadoop-resourcemanager:
  file.managed:
    - source: salt://hadoop/files/{{ hadoop.initscript }}
    - user: root
    - group: root
    - mode: '755'
    - template: jinja
    - context:
      hadoop_svc: resourcemanager
      hadoop_user: yarn
      hadoop_home: {{ hadoop.hadoop_home }}

hadoop-resourcemanager:
  service.running:
    - enable: True
{% endif %}

{% if yarn.is_nodemanager %}

# add mr-history directories for Hadoop 2
{%- set yarn_site = yarn.config_yarn_site %}
{%- set rald = yarn_site.get('yarn.nodemanager.remote-app-log-dir', '/app-logs') %}

{% for disk in yarn.yarn_data_disks %}
/data/{{ disk }}/yarn:
  file.directory:
    - user: root
    - group: root
    - makedirs: True
/data/{{ disk }}/yarn/local:
  file.directory:
    - user: yarn
    - group: hadoop
    - require:
      - file: /data/{{ disk }}/yarn
/data/{{ disk }}/yarn/logs:
  file.directory:
    - user: yarn
    - group: hadoop
    - require:
      - file: /data/{{ disk }}/yarn
{% endfor %}


{{ hadoop.hadoop_config }}/container-executor.cfg:
  file.managed:
    - unless: test ! -f {{hadoop.hadoop_home}}/bin/container-executor
    - source: salt://hadoop/conf/yarn/container-executor.cfg
    - mode: 644
    - user: root
    - group: root
    - template: jinja
    - context:
      local_disks:
{%- for disk in yarn.yarn_data_disks %}
        - /data/{{ disk }}/yarn/local
{%- endfor %}
      local_log_disk: {{ yarn.first_data_disk }}/yarn/log
      banned_users_list: {{ yarn.banned_users|join(',') }}
      
/etc/init.d/hadoop-nodemanager:
  file.managed:
    - source: salt://hadoop/files/{{ hadoop.initscript }}
    - user: root
    - group: root
    - mode: '755'
    - template: jinja
    - context:
      hadoop_svc: nodemanager
      hadoop_user: yarn
      hadoop_home: {{ hadoop.hadoop_home }}

hadoop-nodemanager:
  service.running:
    - enable: True
{%- endif %}