# hadoop-formula/hadoop/init.sls

{%- from 'hadoop/settings.sls' import hadoop with context %}



vm.swappiness:
  sysctl:
    - present
    - value: 0

vm.overcommit_memory:
  sysctl:
    - present
    - value: 0
    
unpack-hadoop-dist:
  cmd.run:
    - name: curl 'http://apache.arvixe.com/hadoop/common/hadoop-2.7.1/hadoop-2.7.1.tar.gz' | tar xz --no-same-owner
    - cwd: /usr/lib
    - unless: test -d {{ hadoop['hadoop_home'] }}/lib
    
hadoop:
  group.present:
    - gid: 6000
    
create-common-directories:
  file.directory:
    - user: root
    - group: hadoop
    - mode: 775
    - names:
      - /var/log/hadoop
      - /var/run/hadoop
      - /var/lib/hadoop
    - require:
      - group: hadoop
    - makedirs: True
    
create-conf-directories:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - name: /etc/hadoop/conf
    - makedirs: True

{{ hadoop['hadoop_config'] }}/hadoop-env.sh:
  file.managed:
    - source: salt://hadoop/conf/hadoop-env.sh
    - template: jinja
    - mode: 644
    - user: root
    - group: root
    - context:
      java_home: {{ hadoop.java_home }}
      hadoop_home: {{ hadoop.hadoop_home }}
      hadoop_config: {{ hadoop.hadoop_config }}
      
{{ hadoop['hadoop_config'] }}/mount_drives.sh:
  file.managed:
    - source : salt://hadoop/files/mount_drives.sh.jinja
    - template: jinja
    - mode: '755'
    - user: root
    - group: root
    
mount-drives:
  cmd.script:
    - require:
      - file: {{ hadoop['hadoop_config'] }}/mount_drives.sh
    - source: {{ hadoop['hadoop_config'] }}/mount_drives.sh
    - user: root
    - group: root
    - shell: /bin/bash
    - unless: test -d /data/data1