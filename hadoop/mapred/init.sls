{%- from 'hadoop/settings.sls' import hadoop with context %}
{%- from 'hadoop/mapred/settings.sls' import mapred with context %}

{% set username = 'mapred' %}
{% set uid = '6002' %}

{{ hadoop['hadoop_config'] }}/mapred-site.xml:
  file.managed:
    - source: salt://hadoop/conf/mapred/mapred-site.xml
    - template: jinja
    - mode: 644
    
{%- if mapred.is_historyserver %}

include:
  - hadoop.users.mapred

/etc/init.d/hadoop-historyserver:
  file.managed:
    - source: salt://hadoop/files/{{ hadoop.initscript }}
    - user: root
    - group: root
    - mode: '755'
    - template: jinja
    - context:
      hadoop_svc: historyserver
      hadoop_user: mapred
      hadoop_home: {{ hadoop.hadoop_home }}

hadoop-historyserver:
  service.running:
    - enable: True
    
{%- endif %}